var characterList = document.getElementById("characterList");

window.onload = () => {
    //login 
    document.getElementById("login").addEventListener("click", () => {
        //Input node
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;


        //ajax logic
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = () => {
            if(xhr.readyState == XMLHttpRequest.DONE && xhr.status ===200){
                var data = JSON.parse(xhr.responseText);
                console.log(data);
                login(data);
            }
        };
        xhr.open("POST",`login.do?username=${username}&password=${password}`);
        xhr.send();

    });
}

function login(data){
    //if message is a member of the json, it wasa authenticated_failed
    if(data.message){
        document.getElementById("loginMessage").innerHTML ='<span class="label label-danger label-center">Wrong credentials.</span>';
    }
}